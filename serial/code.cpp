#include <iostream>
#include <vector>
#include <string>
#include <chrono>

using namespace std;
using namespace std::chrono;

void read_csv(const string& file_name, vector<vector<float>>& result);
float predict_class(const vector<float>& instance, const vector<vector<float>>& weights);
void normalize(vector<vector<float>>& instances);

int main(int argc, char* argv[])
{
    auto start = high_resolution_clock::now();

    vector<vector<float>> weights, instances;

    string dataset_dir(argv[1]);

    read_csv(dataset_dir + "weights.csv", weights);
    read_csv(dataset_dir + "train.csv", instances);

    normalize(instances);

    long correct_predictions = 0;
    for (long i = 0; i < instances.size(); i++)
    {
        if(predict_class(instances[i], weights) == instances[i][20])
            correct_predictions++;
    }
    
    cout << "Accuracy: " << correct_predictions / (float) instances.size() * 100 << "%\n";

    auto end = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(end - start); 
    cout << duration.count() << "\n";

    return 0;
}