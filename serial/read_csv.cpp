#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

void read_csv(const string& file_name, vector<vector<float>>& result) 
{ 
    fstream fin; 
    string line, word, temp;

    fin.open(file_name, ios::in); 
    getline(fin, line);

    result.clear();
  
    while (getline(fin, line)) { 
  
        result.push_back(vector<float>()); 
   
        stringstream s(line); 
  
        while (getline(s, word, ','))
            result.back().push_back(stof(word)); 
    } 
}