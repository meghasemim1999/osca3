#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct MinMax
{
    float min, max;
};


MinMax find_minmax_on_column(const vector<vector<float>>& instances, int column)
{
    MinMax result;

    result.min = __FLT_MAX__;
    result.max = -__FLT_MAX__;

    for (int i = 0; i < instances.size(); i++)
    {
        if (instances[i][column] > result.max)
        {
            result.max = instances[i][column];
        }

        if (instances[i][column] < result.min)
        {
            result.min = instances[i][column];
        }        
    }

    return result;    
}

void normalize_column(vector<vector<float>>& instances, int column)
{
    MinMax column_minmax = find_minmax_on_column(instances, column);

    for (int i = 0; i < instances.size(); i++)
    {
        instances[i][column] = (instances[i][column] - column_minmax.min) / (column_minmax.max - column_minmax.min); 
    }
}

void normalize(vector<vector<float>>& instances, int start_col = 0, int num = 20)
{
    for (int i = start_col; i < start_col + num; i++)
    {
        normalize_column(instances, i);
    }
}