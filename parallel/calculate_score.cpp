#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

float calculate_score(const vector<float>& instance, const vector<float>& weight)
{
    if (instance.size() != weight.size())
    {
        throw  logic_error("vectors are not in equal size.");
    }
    
    float result = 0;

    for (int i = 0; i < instance.size() - 1; i++)
    {
        result += instance[i] * weight[i];
    }

    result += weight[weight.size() - 1];

    return result;
}

float predict_class(const vector<float>& instance, const vector<vector<float>>& weights)
{
    float best_score = -__FLT_MAX__;
    float best_class = -1;

    for (int i = 0; i < weights.size(); i++)
    {
        float class_score = calculate_score(instance, weights[i]);
        if (class_score > best_score)
        {
            best_score = class_score;
            best_class = i;
        }
    }

    return best_class;    
}