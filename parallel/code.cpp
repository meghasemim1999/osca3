#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <pthread.h>

using namespace std;
using namespace std::chrono;

string dataset_dir;
long correct_predicted;
vector<vector<float>> weights, all_instances;
pthread_mutex_t correct_predicted_mutex, all_instances_mutex;

void read_csv(const string& file_name, vector<vector<float>>& result);
float predict_class(const vector<float>& instance, const vector<vector<float>>& weights);
void normalize(vector<vector<float>>& instances, int start_col = 0, int num = 20);

#define NUM_THREADS 4

void* read_csv_parts(void* args)
{
    long dataset_number = ((long)args);
    vector<vector<float>> instances;
    read_csv(dataset_dir + "train_" + to_string(dataset_number) + ".csv", instances);

    pthread_mutex_lock(&all_instances_mutex);
    all_instances.insert(all_instances.end(), instances.begin(), instances.end());
    pthread_mutex_unlock(&all_instances_mutex);

    pthread_exit(NULL);
}

void* normalize_some_cols(void* args)
{
    long col_part_num = ((long)args);

    normalize(all_instances, col_part_num * 20 / NUM_THREADS, 20 / NUM_THREADS);

    pthread_exit(NULL);
}

void* process_part(void* args)
{
    long part_num = (long)args;

    long corrects = 0;
    int num_of_instances = all_instances.size();
    long start = part_num * num_of_instances / NUM_THREADS, end = start + num_of_instances / NUM_THREADS;
    for (long i = start; i < end; i++)
    {
        if(predict_class(all_instances[i], weights) == all_instances[i][20])
        {
            corrects++;
        }
    }

    pthread_mutex_lock(&correct_predicted_mutex);
    correct_predicted += corrects;
    pthread_mutex_unlock(&correct_predicted_mutex);

    pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
    auto start = high_resolution_clock::now();

    pthread_t threads[4];

    dataset_dir = argv[1];

    read_csv(dataset_dir + "weights.csv", weights);

    correct_predicted = 0;

    pthread_mutex_init(&all_instances_mutex, NULL);
    pthread_mutex_init(&correct_predicted_mutex, NULL);

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&threads[i], NULL, read_csv_parts, (void*)i);
    }

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&threads[i], NULL, normalize_some_cols, (void*)i);
    }

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&threads[i], NULL, process_part, (void*)i);
    }

    for (long i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    cout << "Accuracy: " << correct_predicted / (float) 2000 * 100 << "%\n";

    auto end = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(end - start); 
    cout << duration.count() << "\n";

    return 0;
}